pic_name = 'pic1.png';
pic = rgb2gray(imread(pic_name));
figure(100);
imshow(uint8(pic));
imwrite(uint8(pic),'pic0.png')

PicMatrix= double(pic);

[U, S, V] = svd(PicMatrix);

D=diag(S);

figure(1);
scatter(1:length(S),D,10,"filled");
ylabel ('\sigma');
xlabel('i')
fontsize(gca,24,"points")
x0=100;
y0=100;
width=500;
height=500;
set(gcf,'position',[x0,y0,width,height])

figure(2);
lim=500;
scatter(1:lim,D(1:lim),10,"filled");
ylabel ('\sigma');
xlabel('i')
fontsize(gca,24,"points")
x0=100;
y0=100;
width=500;
height=500;
set(gcf,'position',[x0,y0,width,height])

figure(3);
lim=100;
scatter(1:lim,D(1:lim),10,"filled");
ylabel ('\sigma');
xlabel('i')
fontsize(gca,24,"points")
x0=100;
y0=100;
width=500;
height=500;
set(gcf,'position',[x0,y0,width,height])

N=8;
cut=zeros(1,N);

for i=1:N
    cut(i)=floor(length(S)/2^i);
end

for i=cut
    U_c = U(:, 1:i);
    S_c = S(1:i, 1:i);
    V_c = V(:, 1:i);
    
    PicMatrix_cut = U_c*S_c*V_c';

    figure('Name',num2str(i) +" singluar values left",'NumberTitle','off');
    imshow(uint8(PicMatrix_cut));
    imwrite(uint8(PicMatrix_cut),num2str(i)+".png")
end
